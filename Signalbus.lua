local Signalbus = Klass()

function Signalbus:init()
  self._all_callbacks = {}
end

function Signalbus:on(name, callback)
  if not self._all_callbacks[name] then
    self._all_callbacks[name] = {}
  end

  -- TODO: check if callback is already registered
  table.insert(self._all_callbacks[name], callback)
end

-- TODO: implement :off

function Signalbus:emit(name, ...)
  local callbacks = self._all_callbacks[name]
  if callbacks then
    for _, callback in ipairs(callbacks) do
      callback(...)
    end
  end
end

return Signalbus