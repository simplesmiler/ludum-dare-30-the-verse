-- global
__watch__ = {
  ["1. Camera control"] = "Click on empty space  with right or left button, and then drag",
  ["2. Fleet control"] = "Hover over system, click and drag. Use [Shift] to avoid hover and force camera movement"
}
util = require("util")
Klass = require("Klass")
Entity = require("Entity")
Roster = require("Roster")
Quaternion = require("Quaternion")
Signalbus = require("Signalbus")

-- util
math.randomseed(os.time())

-- game data
local conf = require("conf")
local pieces = require("pieces")

-- state data
local shared = {
  volume = 0,
  const = {
    min_battletime = 1,     max_battletime = 4,
    min_siegetime = 2,      max_siegetime = 3,
    min_poplim = 1,         max_poplim = 9,
    min_poptime = 8,        max_poptime = 20,
    -- min_richness = 1,       max_richness = 9,
    min_productiontime = 2, max_productiontime = 4,
    min_aitime = 1,         max_aitime = 2,
  },
  fonts = {}, -- in love.load
  sounds = {},
  images = {},
  signalbus = Signalbus(),
  names = require("names")
}
local context = nil
local contexts = {
  ["menu"] = require("cMenu")(shared, conf),
  ["custom"] = require("cCustom")(shared, conf),
  ["simulation"] = require("cSimulation")(shared, conf),
}

function love.load()
  -- -- resources
  shared.fonts["systems"] = love.graphics.newFont("assets/fonts/Oxygen/Oxygen-Light.ttf", 14)
  shared.fonts["title"] = love.graphics.newFont("assets/fonts/Oxygen/Oxygen-Light.ttf", 32)
  love.graphics.setFont(shared.fonts["systems"])

  shared.sounds["backinit"] = love.audio.newSource("assets/sounds/backinit.ogg", "static")
  shared.sounds["backloop"] = love.audio.newSource("assets/sounds/backloop.ogg", "static")
  shared.sounds["backloop"]:setLooping(true)
  shared.sounds["backloop"]:setVolume(0)
  shared.sounds["backloop"]:play()
  shared.sounds["backloop"]:seek(util.frand(0, 59)) -- approx length of the track
  
  shared.images["logo"] = love.graphics.newImage("assets/images/logo.png")

  shared.signalbus:on("start", function (options)
    local simulation = contexts["simulation"]
    simulation:reset(options)
    context = simulation
  end)

  shared.signalbus:on("menu", function (options)
    local menu = contexts["menu"]
    context = menu
  end)

  shared.signalbus:on("custom", function (options)
    local custom = contexts["custom"]
    context = custom
  end)

  context = contexts["menu"]
end

local accum = 0
local total = 0
local step = 1/60
function love.update(dt)
  if shared.volume < 1 then
    shared.volume = shared.volume + 0.1 * dt
    shared.sounds["backloop"]:setVolume(shared.volume)
  end
  -- print("")
  accum = accum + dt
  local mx, my = love.mouse.getPosition()
  if context.mouseposition then context:mouseposition(mx, my) end

  while accum > step do
    if context.update then context:update(step, total) end
    total = total + step
    accum = accum - step
  end
end

function love.draw()
  love.graphics.setBackgroundColor(20, 20, 28)
  if context.draw then context:draw() end

  local names = {}
  for name, value in pairs(__watch__) do
    table.insert(names, name)
  end
  table.sort(names, function (name1, name2) return name1 < name2 end)

  -- local watchline = ""
  -- for id, name in ipairs(names) do
  --   local value = __watch__[name]
  --   if value then
  --     watchline = watchline .. name .. ": " .. tostring(value) .. "\n"
  --   end
  -- end

  -- love.graphics.setColor(255, 255, 255)
  -- love.graphics.print(watchline)
end

function love.keypressed(key, isrepeat)
  if key == "q" then love.event.quit() end

  if context.keypressed then
    context:keypressed(key, isrepeat)
  else
    print("Keyboard input ignored")
  end
end

function love.keyreleased(key)
  if context.keyreleased then
    context:keyreleased(key)
  else
    print("Keyboard input ignored")
  end
end

function love.mousepressed(x, y, button)
  if context.mousepressed then
    context:mousepressed(x, y, button)
  else
    print("Mouse input ignored")
  end
end

function love.mousereleased(x, y, button)
  if context.mousereleased then
    context:mousereleased(x, y, button)
  else
    print("Mouse input ignored")
  end
end