local pieces = require("pieces")
local gen = require("gen")

local cSimulation = Klass()

-- oh shi~, so dirty
local function _getDistance(s1, s2)
  local connected = false
  for _, conn in ipairs(s1.lanes) do
    if s2 == conn then
      connected = true
      break
    end
  end
  local dist = util.edist3v(s1.position, s2.position)
  if connected then dist = dist / 4 end
  return dist
end

function cSimulation:reset(options)
  local me_roster = self -- HACKZ
  options.avgdist = options.avgdist or 0.5
  options.player_ai = options.player_ai ~= nil and options.player_ai or false
  options.zen = options.zen ~= nil and options.zen or false
  if options.spinning == nil then options.spinning = true end

  self.state.players["player"].ai = options.player_ai
  self.state.spinning = options.spinning
  self.state.zen = options.zen

  local randomName = function ()
    local letters = string.char(math.random(65, 90), math.random(65, 90), math.random(65, 90));
    local numbers = math.random(0, 9) .. math.random(0, 9) .. math.random(0, 9)
    return letters .. "-" .. numbers
  end

  local roster = Roster(Entity)
  self.roster = roster
  local points = gen.generatePoints(1, options.avgdist)

  function planet_script (self, others, state, signalbus)
    if self.owner.name == "neutral" then
      if self.ships.our == 0 and self.ships.enemy > 0 then
        self.owner = state.players["enemy"]
        self.ships.enemy = self.ships.enemy - 1
        self.population.current = self.population.current + 1
      elseif self.ships.enemy == 0 and self.ships.our > 0 then
        self.owner = state.players["player"]
        self.ships.our = self.ships.our - 1
        self.population.current = self.population.current + 1
      else
        -- shouldn't really happen
      end
    elseif self.owner.name == "player" then
      if self.ships.enemy <= self.ships.our then
        if not self.timers["production_tick"] then
          self.timers["production_tick"] = util.frand(me_roster.shared.const.max_productiontime, me_roster.shared.const.max_productiontime)
        end
      end
      if self.ships.enemy > 0 then
        -- either battle or siege
        if self.ships.our == 0 then
          if not self.timers["siege_tick"] then
            self.timers["siege_tick"] = util.frand(me_roster.shared.const.min_siegetime, me_roster.shared.const.max_siegetime)
          end
        else
          if not self.timers["battle_tick"] then
            self.timers["battle_tick"] = util.frand(me_roster.shared.const.min_battletime, me_roster.shared.const.max_battletime)
          end
        end
      else
        if not self.timers["pop_tick"] then
          self.timers["pop_tick"] = util.frand(me_roster.shared.const.min_poptime, me_roster.shared.const.max_poptime)
        end
      end
    elseif self.owner.name == "enemy" then
      if self.ships.our <= self.ships.enemy then
        if not self.timers["production_tick"] then
          self.timers["production_tick"] = util.frand(me_roster.shared.const.max_productiontime, me_roster.shared.const.max_productiontime)
        end
      end
      if self.ships.our > 0 then
        -- either battle or siege
        if self.ships.enemy == 0 then
          if not self.timers["siege_tick"] then
            self.timers["siege_tick"] = util.frand(me_roster.shared.const.min_siegetime, me_roster.shared.const.max_siegetime)
          end
        else
          if not self.timers["battle_tick"] then
            self.timers["battle_tick"] = util.frand(me_roster.shared.const.min_battletime, me_roster.shared.const.max_battletime)
          end
        end
      else
        if not self.timers["pop_tick"] then
          self.timers["pop_tick"] = util.frand(me_roster.shared.const.min_poptime, me_roster.shared.const.max_poptime)
        end
        if not self.timers["production_tick"] then
          self.timers["production_tick"] = util.frand(me_roster.shared.const.max_productiontime, me_roster.shared.const.max_productiontime)
        end
      end
    end

    if self.timers["siege_tick"] and self.timers["siege_tick"] < 0 then
      self.timers["siege_tick"] = nil
      print("siege on " .. self.name)
      if self.ships.our > 0 and self.ships.enemy == 0 then
        if self.population.current > 0 then
          self.population.current = self.population.current - 1
        else
          self.owner = state.players["player"]
          self.population.current = self.population.current + 1
          self.ships.our = self.ships.our - 1
        end
      elseif self.ships.enemy > 0 and self.ships.our == 0 then
        if self.population.current > 0 then
          self.population.current = self.population.current - 1
        else
          self.owner = state.players["enemy"]
          self.ships.enemy = self.ships.enemy - 1
          self.population.current = self.population.current + 1
        end
      end
    end

    if self.timers["battle_tick"] and self.timers["battle_tick"] < 0 then
      self.timers["battle_tick"] = nil

      -- IMP: change?
      if self.ships.enemy > self.ships.our then
        self.ships.enemy = math.max(0, self.ships.enemy - 1)
        self.ships.our = math.max(0, self.ships.our - 2)
      elseif self.ships.our > self.ships.enemy then
        self.ships.enemy = math.max(0, self.ships.enemy - 2)
        self.ships.our = math.max(0, self.ships.our - 1)
      else
        self.ships.enemy = math.max(0, self.ships.enemy - 1)
        self.ships.our = math.max(0, self.ships.our - 1)
      end

    end

    if self.timers["pop_tick"] and self.timers["pop_tick"] < 0 then
      self.timers["pop_tick"] = nil

      -- TODO: may be affected by bonuses
      local inc = 1
      if self.owner.name == "player" and self.ships.enemy == 0 then
        self.population.current = math.min(self.population.current + inc, self.population.limit)
      elseif self.owner.name == "enemy" and self.ships.our == 0 then
        self.population.current = math.min(self.population.current + inc, self.population.limit)
      end
    end

    if self.timers["production_tick"] and self.timers["production_tick"] < 0 then
      self.timers["production_tick"] = nil
      -- TODO: may be affected by bonuses
      local inc = 1
      if self.owner.name == "player" then
        self.ships.our = self.ships.our + inc
      elseif self.owner.name == "enemy" then
        self.ships.enemy = self.ships.enemy + inc
      end
    end
  end

  for id, point in ipairs(points) do
    local entity = self.roster:create("system-" .. id, {
      pieces.tag("logics.system"),
      pieces.position3(unpack(point)),
      pieces.name(randomName()),
      -- pieces.mining(true, math.random(shared.const.min_richness, shared.const.max_richness)),
      pieces.population(0, math.random(self.shared.const.min_poplim, self.shared.const.max_poplim)),
      pieces.store({}),
      pieces.lanes({}),
      pieces.closest({}),
      pieces.stations(),
      pieces.ships(0, 0),
      pieces.timers({}),
      pieces.owner(self.state.players["neutral"]), -- DIRTY
      pieces.script(planet_script)
    })
  end


  local systems = self.roster:find({"logics.system"})

  -- VERY INEFFECTIVE!
  local closest = {} -- point -> n closest
  for id1, s1 in ipairs(systems) do
    local dst = {}
    for id2, s2 in ipairs(systems) do
      table.insert(dst, {
        util.edist3v(s1.position, s2.position),
        s2
      })
    end
    table.sort(dst, function (a, b)
      return a[1] < b[1]
    end)

    for i = 2, 3 do -- starts from 2 to avoid self referencing
      table.insert(s1.lanes, dst[i][2])
      table.insert(dst[i][2].lanes, s1)
    end

    for i = 2, 5 do -- starts from 2 to avoid self referencing
      table.insert(s1.closest, dst[i][2])
      table.insert(dst[i][2].closest, s1)
    end
  end

  -- remove duplicates
  for _, system in ipairs(systems) do
    system.lanes = util.unique(system.lanes)
    system.closest = util.unique(system.closest)
  end





  local player_system = self.roster:get("system-" .. 1)
  player_system.owner = self.state.players["player"]
  player_system.population.current = 2
  player_system.population.limit = 5
  player_system.ships.our = 2

  local dist = 0
  local enemy_system = nil
  while dist < 1 do
    enemy_system = self.roster:get("system-" .. math.random(#points))
    if enemy_system ~= player_system then
      dist = util.edist3v(player_system.position, enemy_system.position)
    end
    print("pick enemy_system", dist)
  end
  enemy_system.owner = self.state.players["enemy"]
  enemy_system.population.current = 2
  enemy_system.population.limit = 5
  enemy_system.ships.enemy = 2
  print("Generated:", #points)
end

function cSimulation:init(shared, conf)
  self.shared = shared
  self.conf = conf
  self.roster = Roster(Entity) -- relies on Roster and Entity being present in global
  self.state = {
    ai_timer = util.frand(self.shared.const.min_aitime, self.shared.const.max_aitime),
    zen = false,
    players = {
      ["player"] = {
        name = "player", ai = true,
        our_ships = "our",
        enemy_ships = "enemy",
        enemy = "enemy",
      },
      ["enemy"] = {
        name = "enemy", ai = true,
        our_ships = "enemy",
        enemy_ships = "our",
        enemy = "player",
      },
      ["neutral"] = {
        name = "neutral", ai = false
      }
    }
  }
  self.camera = {
    scale = -8, -- [-7 .. -10]
    origin = Quaternion.fromaxis({0, 0, 1, 0}), -- real camera quaternion
    position = Quaternion.fromaxis({0, 0, 1, 0}), -- visible camera quaternion, affected by dragging and rotation

    -- async
    forced = false,
    dragging = false,
    rotation = false,
  }
  self.mouse = {
    origin = {0, 0},
    delta = {0, 0}
  }
  self.grab = {
    active = false,
    from = nil,
    number = 0
  }
  -- self.hover = nil
  self.hover = {
    system = nil,
    offset = {0, 0},
    number = 0
  }

  -- async
  self.paused = false


  -- internal
  self._last_fleet_id = 0
  self._visible_systems = {}

  -- mechanics
  self.signalbus = Signalbus()
  -- self.signalbus:on("produce_warship", function (player, system)
  --   system.ships.our = system.ships.our + 1
  --   player.warships = player.warships + 1
  -- end)
  self.signalbus:on("send_fleet", function (player, from, to, number)
    if to == from then
      print("Same system")
      return
    end

    number = math.min(number, from.ships[player.our_ships])
    from.ships[player.our_ships] = from.ships[player.our_ships] - number

    local x1, y1, z1 = unpack(from.position)
    local x2, y2, z2 = unpack(to.position)

    local arrive = 10 * _getDistance(to, from)

    self._last_fleet_id = self._last_fleet_id + 1
    local fleet = self.roster:create("fleet-" .. self._last_fleet_id, {
      pieces.tag("logics.fleet"),
      pieces.store({
        number = number,
        from = from,
        to = to,
        arrive = arrive
      }),
      pieces.position3(unpack(from.position)),
      pieces.owner(player),
      pieces.timers({ arrive_tick = arrive }), -- IMP: affected by distance
      pieces.script(function (self, others, state, signalbus)
        if self.timers["arrive_tick"] then
          self.position = util.interp3v(self.store.from.position, self.store.to.position, 1 - self.timers.arrive_tick / self.store.arrive)
        end
        if self.timers["arrive_tick"] and self.timers["arrive_tick"] < 0 then
          if self.owner.name == player.name then
            self.store.to.ships[player.our_ships] = to.ships[player.our_ships] + self.store.number
          elseif self.owner.name == player.enemy then
            self.store.to.ships[player.enemy_ships] = to.ships[player.enemy_ships] + self.store.number
          end
          signalbus:emit("consume_fleet", self)
          self.timers["arrive_tick"] = nil
        end
      end)
    })
  end)

  self.signalbus:on("consume_fleet", function (fleet)
    self.roster:unreg(fleet)
  end)
  -- IMP: destroy_ship
  -- IMP: produce_station
  -- IMP: destroy_station
end

-- TODO: implement :onLeave to stop dragging and rotation

function cSimulation:update(dt, total)
  -- __watch__["camera.dragging"] = self.camera.dragging
  -- __watch__["camera.rotation"] = self.camera.rotation
  -- __watch__["camera.forced"] = self.camera.forced

  -- __watch__["grab.active"] = self.grab.active
  if self.grab.active then
    -- __watch__["grab.number"] = self.grab.number
    -- __watch__["grab.from"] = self.grab.from.name
  else
    -- __watch__["grab.number"] = nil
  end

  local with_timers = self.roster:find({"timers"})
  for _, entity in ipairs(with_timers) do
    for name, timer in pairs(entity.timers) do
      entity.timers[name] = timer - dt
    end
  end

  local scripted = self.roster:find({"script"})
  for _, entity in ipairs(scripted) do
    entity.script(entity, self.roster, self.state, self.signalbus)
  end

  
  local function AI(roster, signalbus, player, side)
    local systems = roster:find({"logics.system"})

    --  {from, to, cost, benefit, amount}, ...

    -- tries to capture an empty system
    local neutral = {}
    -- tries to capture weak enemy world
    local enemy = {}
    -- tries to keep units equal
    local inner = {}

    -- DIRTY AND VERY INEFFECTIVE!!!
    for _, s1 in ipairs(systems) do
      if s1.owner.name == player.name then
        if s1.ships[player.our_ships] > 0 then
          for _, s2 in ipairs(s1.closest) do
            if s2.owner.name == "neutral" then
              -- __watch__["distance"] = _getDistance(s1, s2)
              local cost = 2 * _getDistance(s1, s2)
              local benefit = 0.1 * math.sqrt(s2.population.limit)
              table.insert(neutral, {s1, s2, cost, benefit})
            elseif s2.owner.name == player.enemy then
              local cost = 2   * _getDistance(s1, s2)
                         + 0.1 * math.sqrt(s2.population.limit)
                         + 0.5 * math.sqrt(s2.ships[player.enemy_ships])

              local benefit = 0.1
              if s2.ships[player.enemy_ships] < s2.ships[player.our_ships] / 2 then
                benefit = benefit + s2.ships[player.our_ships] / (s2.ships[player.enemy_ships] + 1)
              elseif s2.ships[player.enemy_ships] < s2.ships[player.our_ships] then
                benefit = benefit + 0.3 * s2.ships[player.our_ships] / (s2.ships[player.enemy_ships] + 1)
              end

              table.insert(enemy, {s1, s2, cost, benefit})
            elseif s2.owner.name == player.name then
              local cost = 1 * _getDistance(s1, s2)
                         + 0.5 * math.sqrt(s2.ships[player.enemy_ships])
              local benefit = 0.1
              if s2.ships[player.enemy_ships] < s2.ships[player.our_ships] + s1.ships[player.our_ships] then
                benefit = benefit + 5 -- defensive enough?
              elseif s2.ships[player.our_ships] < s1.ships[player.our_ships] * 0.9 then
                benefit = benefit + 2 -- spread
              end
              table.insert(inner, {s1, s2, cost, benefit})
            end
          end
        end
      end
    end

    table.sort(neutral, function (c1, c2)
      return c1[4] / c1[3] > c2[4] / c2[3]
    end)
    table.sort(enemy, function (c1, c2)
      return c1[4] / c1[3] > c2[4] / c2[3]
    end)
    table.sort(inner, function (c1, c2)
      return c1[4] / c1[3] > c2[4] / c2[3]
    end)

    return function ()
      if #neutral > 0 then
        local choise = math.random(#neutral)
        choise = 1 + math.fmod(choise - 1, 3) -- randomly pick one of three most valuable things
        local from, to, cost, benefit = unpack(neutral[choise])
        signalbus:emit("send_fleet", player, from, to, 1) -- probe
      elseif #enemy > 0 or #inner > 0 then
        local a = math.random(2)
        if #enemy > 0 and a == 1 then -- enemy
          local choise = math.random(#enemy)
          choise = 1 + math.fmod(choise - 1, 3) -- randomly pick one of three most valuable things
          local from, to, cost, benefit = unpack(enemy[choise])
          local ammout = math.min(math.floor(to.ships[player.enemy_ships] * 2), from.ships[player.our_ships])
          signalbus:emit("send_fleet", player, from, to, ammout)
        elseif #inner > 0 then
          local choise = math.random(#inner)
          choise = 1 + math.fmod(choise - 1, 3) -- randomly pick one of three most valuable things
          local from, to, cost, benefit = unpack(inner[choise])
          local ammout = 0
          if to.ships[player.enemy_ships] == 0 then
            ammout = math.floor(0.5 * (from.ships[player.our_ships] - to.ships[player.enemy_ships]))
          else
            ammout = math.min(math.floor(to.ships[player.enemy_ships] * 2), from.ships[player.our_ships])
          end
          signalbus:emit("send_fleet", player, from, to, ammout)
        end
      end
    end
  end

  -- TODO: perform ai only once [twice] a second?
  -- TODO: gather intel once in five seconds, and perform ai every now and then?
  -- TODO: keep intel updated, react to events?
  self.state.ai_timer = self.state.ai_timer - dt
  if self.state.ai_timer < 0 then
    print("perform AI")
    self.state.ai_timer = util.frand(self.shared.const.min_aitime, self.shared.const.max_aitime)

    local actions = {}
    if self.state.players["player"].ai then
      table.insert(actions, AI(self.roster, self.signalbus, self.state.players["player"]))
    end
    if self.state.players["enemy"].ai then
      table.insert(actions, AI(self.roster, self.signalbus, self.state.players["enemy"]))
    end
    for _, action in ipairs(actions) do action() end
  end

  -- update hover
  local ox, oy = unpack(self.mouse.origin)
  local dx, dy = unpack(self.mouse.delta)
  local closest, distance, offset, number = nil, math.huge, {0, 0}, 0

  local i = 0
  for system, vispos in pairs(self._visible_systems) do
    i = i + 1
    local visx, visy = unpack(vispos)
    local dist = util.edist2(visx, visy, dx + ox, dy + oy)
    if dist < 50 and dist < distance then
      closest, distance, offset = system, dist, {(dx + ox) - visx, (dy + oy) - visy}
    end
  end
  self.hover.system = closest
  self.hover.offset = offset
  if closest then
    local part = math.min(math.max(0, 40 + offset[1]), 80)/ 80
    self.hover.number = math.max(1, math.floor(0.5 + part * closest.ships.our))
  else
    self.hover.number = 0
  end
  -- __watch__["camera.hover"] = (self.hover.system and self.hover.system.name or "") .. " [" .. self.hover.number .. "]"

  -- update camera
  if self.camera.rotation then
    local dx, dy = unpack(self.mouse.delta)
    local ox, oy = unpack(self.mouse.origin)
    local cx, cy = 960/2, 540/2
    local oangle = math.atan2(ox - cx, oy - cy)
    local nangle = math.atan2(ox - cx + dx, oy - cy + dy)
    local rr = oangle - nangle
    local quaternion = Quaternion.mulmany({
      Quaternion.conj(self.camera.origin),
      Quaternion.fromaxis({rr,  0,  0,  1}),
      self.camera.origin,
    })
    self.camera.position = Quaternion.mul(self.camera.origin, quaternion)
  elseif self.camera.dragging then
    local dx, dy = unpack(self.mouse.delta)
    local speed = 250 * math.pow(2, self.camera.scale) -- 1 at -8, 0.2 at -10
    local rx = speed * 2 * dx / (960/2)
    local ry = speed * 1 * dy / (540/2)
    local quaternion = Quaternion.mulmany({
      Quaternion.conj(self.camera.origin),
      Quaternion.fromaxis({rx,  0,  1,  0}),
      Quaternion.fromaxis({ry, -1,  0,  0}),
      self.camera.origin,
    })
    self.camera.position = Quaternion.mul(self.camera.origin, quaternion)
  else
    -- autospin
    if self.state.spinning then
      local ax, ay, az = -0.5, 1, -1
      local s = math.sqrt(ax*ax + ay*ay + az*az)
      local quaternion = Quaternion.mulmany({
        Quaternion.conj(self.camera.origin),
        Quaternion.fromaxis({dt * 0.03, ax/s,  ay/s,  az/s}),
        self.camera.origin,
      })
      self.camera.origin = Quaternion.norm(Quaternion.mul(self.camera.origin, quaternion))
      self.camera.position = self.camera.origin
    end
  end

end

function cSimulation:getColorByOwner(owner)
  if owner.name == "player" then
    return {127, 255, 127}
  elseif owner.name == "neutral" then
    return {127, 127, 127}
  elseif owner.name == "enemy" then
    return {255, 127, 127}
  end

  assert(false, "WAT")
end

function cSimulation:draw()
  local systems = self.roster:find({"logics.system"})

  local mox, moy = unpack(self.mouse.origin)
  local mdx, mdy = unpack(self.mouse.delta)
  local centerx, centery = 960 / 2, 540 / 2
  local scale = math.pow(0.5, self.camera.scale)

  self._visible_systems = {} -- DIRTY!
  -- TODO: split cycles for speed?
  for _, system in ipairs(systems) do
    local x, y, z = unpack(Quaternion.apply(self.camera.position, system.position))

    local lanesPoints = {} -- {x, y}, ...
    for _, lane in ipairs(system.lanes) do
      local cx, cy, cz = unpack(Quaternion.apply(self.camera.position, lane.position))
      table.insert(lanesPoints, {cx, cy})
    end

    local r, hover = nil, false
    local color, opacity = nil, nil

    if z > 0 then -- system is close (0 .. 1]
      opacity = 64 + z * (255 - 64)
    else -- system is far [-1 .. 0]
      opacity = 64
    end
    
    if system == self.hover.system and not self.camera.forced then
      radius = math.max(1, z * 2)
      hover = true
      color = {127, 127, 255}
    else
      radius = math.max(1, z * 2)
      color = self:getColorByOwner(system.owner)
    end

    assert(radius ~= nil)
    assert(color ~= nil)
    assert(opacity ~= nil)

    local r, g, b = unpack(color)
    love.graphics.setColor(r, g, b, opacity)

    local visx = centerx + scale * x
    local visy = centery + scale * y

    if visx > -64 and visx < 960 + 64  and visy > -64 and visy < 540 + 64 then
      if z > 0 then
        self._visible_systems[system] = {visx, visy}
      end

      love.graphics.circle("line", visx, visy, radius)
      love.graphics.circle("fill", visx, visy, radius)

      love.graphics.setFont(self.shared.fonts["systems"])

      if not self.state.zen then
        local line2 = string.format("[%d/%d] %s",
          system.population.current,
          system.population.limit,
          system.name)
        love.graphics.printf(line2, visx, visy,
          128, "center", 0, -- width, alignment, orientation
          1, 1, 64, -24) -- scale, offset

        if hover and not self.grab.active and self.hover.system.ships.our > 0 then
          local width = self.hover.number / self.hover.system.ships.our * 80
          love.graphics.rectangle("line", visx - 40, visy - 5 + 16, 80, 10)
          love.graphics.rectangle("fill", visx - 40, visy - 5 + 16, width, 10)
        else
          -- local line1 = string.format("%d : %d", system.ships.our, system.ships.enemy)
          -- love.graphics.setColor(r, g, b, opacity)
          love.graphics.setColor(r, g, b, opacity)
          love.graphics.printf("~", visx, visy,
            128, "center", 0, -- width, alignment, orientation
            1, 1, 64, -6) -- scale, offset

          local r1, g1, b1, r2, g2, b2
          if system.owner.name == "player" then
            r1, g1, b1 = unpack(self:getColorByOwner(self.state.players.player))
            if system.ships.enemy > 0 then
              r2, g2, b2 = unpack(self:getColorByOwner(self.state.players.enemy))
            else
              r2, g2, b2 = r, g, b
            end
          elseif system.owner.name == "enemy" then
            if system.ships.our > 0 then
              r1, g1, b1 = unpack(self:getColorByOwner(self.state.players.player))
            else
              r1, g1, b1 = r, g, b
            end
            r2, g2, b2 = unpack(self:getColorByOwner(self.state.players.enemy))
          else -- neutral
            r1, g1, b1 = r, g, b
            r2, g2, b2 = r, g, b
          end

          love.graphics.setColor(r1, g1, b1, opacity)
          love.graphics.printf(system.ships.our, visx, visy,
            64, "right", 0, -- width, alignment, orientation
            1, 1, 64 + 6, -6) -- scale, offset
          love.graphics.setColor(r2, g2, b2, opacity)
          love.graphics.printf(system.ships.enemy, visx, visy,
            64, "left", 0, -- width, alignment, orientation
            1, 1, 0 - 6, -6) -- scale, offset

        end
      end

      local opacity_coef = (system.owner.name == "neutral") and 1/8 or 1/4

      love.graphics.setColor(r, g, b, opacity * opacity_coef)
      for _, point in ipairs(lanesPoints) do
        local px, py = unpack(point)
        local vispx = centerx + scale * px
        local vispy = centery + scale * py
        love.graphics.line(visx, visy, vispx, vispy)
      end
    end
  end

  -- done drawing systems

  if self.grab.active then
    local points = nil
    local x1, y1, z1 = unpack(Quaternion.apply(self.camera.position, self.grab.from.position))
    local visx1 = centerx + scale * x1
    local visy1 = centery + scale * y1

    if self.hover.system then
      local x2, y2, z2 = unpack(Quaternion.apply(self.camera.position, self.hover.system.position))
      local visx2 = centerx + scale * x2
      local visy2 = centery + scale * y2
      points = {visx1, visy1, visx2, visy2}
    else
      points = {visx1, visy1, mox + mdx, moy + mdy}
    end
    assert(points ~= nil)
    love.graphics.setColor(127, 127, 255)
    love.graphics.line(points)
  end

  local fleets = self.roster:find({"logics.fleet"})

  for _, fleet in ipairs(fleets) do
    local x, y, z = unpack(Quaternion.apply(self.camera.position, fleet.position))

    local opacity, radius = nil, nil
    if z > 0 then -- system is close (0 .. 1]
      opacity = 64 + z * (255 - 64)
      radius = math.min(20, math.max(2, (0.5 + z/2) * fleet.store.number))
    else -- system is far [-1 .. 0]
      opacity = 64
      radius = math.min(10, math.max(2, (1.5 + z/2) * fleet.store.number))
    end
    assert(opacity ~= nil)
    assert(radius ~= nil)

    local r, g, b = unpack(self:getColorByOwner(fleet.owner))
    love.graphics.setColor(r, g, b, opacity)

    local visx = centerx + scale * x
    local visy = centery + scale * y
    love.graphics.circle("line", visx, visy, radius)
  end


end

function cSimulation:keypressed(key, isrepeat)
  if key == "escape" then
    self.shared.signalbus:emit("menu")
  end
  if key == "lshift" or key == "rshift" then
    self.camera.forced = true
  end
end

function cSimulation:keyreleased(key)
  if key == "lshift" or key == "rshift" then
    self.camera.forced = false
  end
end

function cSimulation:mousepressed(x, y, button)
  if not self.camera.dragging and not self.camera.rotation then

    -- scale isn't affected by hover
    if button == "wd" then
      self.camera.scale = math.min(-7, self.camera.scale + 0.1)
    elseif button == "wu" then
      self.camera.scale = math.max(-10, self.camera.scale - 0.1)
    else
      self.mouse.origin = {x, y}
      self.mouse.delta = {0, 0}
    end

    if not self.camera.forced and self.hover.system then
      if button == "l" and self.hover.system.ships.our > 0 then
        self.grab.active = true
        self.grab.from = self.hover.system
        self.grab.number = self.hover.number
      end
    else
      if button == "l" then self.camera.dragging = true end
      if button == "r" then self.camera.rotation = true end
    end
  else
    -- pass?
  end
end

function cSimulation:mousereleased(x, y, button)
  if not self.camera.dragging and not self.camera.rotation then
    self.mouse.origin = {0, 0}
    self.mouse.delta = {x, y}
  end

  if self.grab.active then
    if button == "l" then
      if self.hover.system then
        self.signalbus:emit("send_fleet", self.state.players["player"], self.grab.from, self.hover.system, self.grab.number)
      end
      self.grab.active = false
    end
  end

  if self.camera.dragging then
    if button == "l" then
      self.camera.dragging = false
      self.camera.origin = Quaternion.norm(self.camera.position)
      self.mouse.origin = {0, 0}
      self.mouse.delta = {x, y}
    else
      print("ignored mouse input")
    end
  elseif self.camera.rotation then
    if button == "r" then
      self.camera.rotation = false
      self.camera.origin = Quaternion.norm(self.camera.position)
      self.mouse.origin = {0, 0}
      self.mouse.delta = {x, y}
    else
      print("ignored mouse input")
    end
  end
end

function cSimulation:mouseposition(x, y)
  local ox, oy = unpack(self.mouse.origin)
  self.mouse.delta = {x - ox, y - oy}
end

return cSimulation