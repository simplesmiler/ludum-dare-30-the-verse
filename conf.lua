local conf = {
  width = 960,
  heigth = 540
}

function love.conf(t)
  t.version = "0.9.1"                -- The LÖVE version this game was made for (string)
  t.window.width = conf.width -- t.screen.width in 0.8.0 and earlier
  t.window.height = conf.heigth -- t.screen.height in 0.8.0 and earlier
  t.window.title = "Ludum Dare 30: The Verse"        -- The window title (string)
  -- t.window.fsaa = 0        -- The window title (string)
end

return conf