local pieces = require("pieces")

local cCustom = Klass()

function cCustom:init(shared, conf)
  self.shared = shared
  self.conf = conf

  self.state = {
    worldsize = 's',
    worldsize_button = nil,
    spinning = true,
    ai_assits = false,
    zen = false
  }

  self.roster = Roster(Entity)
  self.roster:create("label-worldsize", {
    pieces.tag("logics.button"),
    pieces.tag("logics.label"),
    pieces.layout(416, 256-32-32-8-8, 128, 32),
    pieces.label("Worldsize"),
  })
  self.state.worldsize_button = self.roster:create("label-worldsize-s", {
    pieces.tag("logics.button"),
    pieces.tag("logics.button.worldsize"),
    pieces.layout(424, 256-32-8, 24, 32),
    pieces.label("S"),
    pieces.action(function (button)
      self.state.worldsize = 's'
      self.state.worldsize_button = button
    end)
  })
  self.roster:create("label-worldsize-m", {
    pieces.tag("logics.button"),
    pieces.tag("logics.button.worldsize"),
    pieces.layout(424+24+8, 256-32-8, 24, 32),
    pieces.label("M"),
    pieces.action(function (button)
      self.state.worldsize = 'm'
      self.state.worldsize_button = button
    end)
  })
  self.roster:create("label-worldsize-l", {
    pieces.tag("logics.button"),
    pieces.tag("logics.button.worldsize"),
    pieces.layout(424+24+8+24+8, 256-32-8, 24, 32),
    pieces.label("L"),
    pieces.action(function (button)
      self.state.worldsize = 'l'
      self.state.worldsize_button = button
    end)
  })
  self.roster:create("label-worldsize-x", {
    pieces.tag("logics.button"),
    pieces.tag("logics.button.worldsize"),
    pieces.layout(424+24+8+24+8+24+8, 256-32-8, 24, 32),
    pieces.label("X"),
    pieces.action(function (button)
      self.state.worldsize = 'x'
      self.state.worldsize_button = button
    end)
  })

  self.roster:create("toggle-ai", {
    pieces.tag("logics.button"),
    pieces.tag("logics.label"),
    pieces.layout(416, 256, 128-24-8, 32),
    pieces.label("AI assist"),
  })
  self.roster:create("toggle-ai-button", {
    pieces.tag("logics.button"),
    pieces.layout(416+128-24, 256, 24, 32),
    pieces.label(""),
    pieces.action(function (button)
      self.state.ai_assits = not self.state.ai_assits
    end)
  })

  self.roster:create("toggle-spin", {
    pieces.tag("logics.button"),
    pieces.tag("logics.label"),
    pieces.layout(416, 256+32+8, 128-24-8, 32),
    pieces.label("World spin"),
  })
  self.roster:create("toggle-spin-button", {
    pieces.tag("logics.button"),
    pieces.layout(416+128-24, 256+32+8, 24, 32),
    pieces.label(""),
    pieces.action(function (button)
      self.state.spinning = not self.state.spinning
    end)
  })

  self.roster:create("toggle-zen", {
    pieces.tag("logics.button"),
    pieces.tag("logics.label"),
    pieces.layout(416, 256+32+8+32+8, 128-24-8, 32),
    pieces.label("Zen"),
  })
  self.roster:create("toggle-zen-button", {
    pieces.tag("logics.button"),
    pieces.layout(416+128-24, 256+32+8+32+8, 24, 32),
    pieces.label(""),
    pieces.action(function (button)
      self.state.zen = not self.state.zen
    end)
  })

  self.roster:create("button-start", {
    pieces.tag("logics.button"),
    pieces.layout(416, 256+32+8+32+8+32+8, 128, 32),
    pieces.label("Start"),
    pieces.description("Go on adventure"),
    pieces.action(function (button)
      local avgdist =
        (self.state.worldsize == 's') and 0.5 or
        (self.state.worldsize == 'm') and 0.4 or
        (self.state.worldsize == 'l') and 0.3 or
        (self.state.worldsize == 'x') and 0.15

      self.shared.signalbus:emit("start", {
        avgdist = avgdist,
        player_ai = self.state.ai_assits,
        spinning = self.state.spinning,
        zen = self.state.zen
      })
    end)
  })

  self.mouse = {
    position = {0, 0},

    hover = nil -- button
  }

  -- body
end

function cCustom:update(dt, total)
  local buttons = self.roster:find({"logics.button"})
  local mx, my = unpack(self.mouse.position)
  self.mouse.hover = nil
  for _, button in ipairs(buttons) do
    local bx, by, bw, bh = unpack(button.layout)
    if mx > bx and mx < bx + bw and my > by and my < by + bh then
      self.mouse.hover = button
      break
    end
  end
end

function cCustom:draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.setFont(self.shared.fonts["title"])
  love.graphics.draw(self.shared.images["logo"], 960/2 - 200, 80)
  love.graphics.printf("The Verse", 0, 16, 960, "center")

  local buttons = self.roster:find({"logics.button"})
  love.graphics.setFont(self.shared.fonts["systems"])
  for _, button in ipairs(buttons) do
    local x, y, w, h = unpack(button.layout)

    if button.tags["logics.button.worldsize"] and self.state.worldsize_button == button then
      love.graphics.setColor(255, 255, 255, 64)
      love.graphics.rectangle("fill", x, y, w, h)
    elseif button.id == "toggle-ai-button" and self.state.ai_assits then
      love.graphics.setColor(255, 255, 255, 64)
      love.graphics.rectangle("fill", x, y, w, h)
    elseif button.id == "toggle-spin-button" and self.state.spinning then
      love.graphics.setColor(255, 255, 255, 64)
      love.graphics.rectangle("fill", x, y, w, h)
    elseif button.id == "toggle-zen-button" and self.state.zen then
      love.graphics.setColor(255, 255, 255, 64)
      love.graphics.rectangle("fill", x, y, w, h)
    elseif not button.tags["logics.label"] then
      love.graphics.setColor(255, 255, 255, 16)
      love.graphics.rectangle("fill", x, y, w, h)
    end
    love.graphics.setColor(255, 255, 255, 255)

    if self.mouse.hover == button and button.description then
      love.graphics.printf(button.description, x, y, w, "center", 0, 1, 1, 0, -6)
    else
      love.graphics.printf(button.label, x, y, w, "center", 0, 1, 1, 0, -6)
    end
  end

  love.graphics.setColor(255, 255, 255, 127)
  local helptext = [[
    Use mouse to control camera (try with [shift])
    Mouse over a system to access fleet
    Drag and drop to send fleet
  ]]
  love.graphics.printf(helptext, 0, 540 - 60, 960, "center")

  love.graphics.setColor(255, 255, 255, math.random(8))
  love.graphics.printf("Denis Karabaza @ Ludum Dare 30", 0, 60, 960, "center")
end




function cCustom:keypressed(key, isrepeat)
  if key == "escape" then
    self.shared.signalbus:emit("menu")
  end
end


function cCustom:mousepressed(x, y, button)
  if button == "l" then
    local buttons = self.roster:find({"logics.button"})
    for _, button in ipairs(buttons) do
      local bx, by, bw, bh = unpack(button.layout)
      if x > bx and x < bx + bw and y > by and y < by + bh then
        if button.action then button.action(button) end
        break
      end
    end
  end
end

function cCustom:mousereleased(x, y, button)
end

function cCustom:mouseposition(x, y)
  self.mouse.position = {x, y}
end

return cCustom