local pieces = {}

-- TODO: assert all arguments

function pieces.tag(tag)
  return function (entity)
    entity.tags[tag] = true
  end
end

function pieces.position3(x, y, z)
  return function (entity)
    entity.position = {x, y, z}
    entity.tags["position3"] = true
  end
end

function pieces.layout(x, y, w, h)
  return function (entity)
    entity.layout = {x, y, w, h}
    entity.tags["layout"] = true
  end
end

function pieces.action(func)
  return function (entity)
    entity.action = func
    entity.tags["action"] = true
  end
end

function pieces.label(label)
  return function (entity)
    entity.label = label
    entity.tags["label"] = true
  end
end

function pieces.description(description)
  return function (entity)
    entity.description = description
    entity.tags["description"] = description
  end
end

function pieces.name(name)
  return function (entity)
    entity.name = name
    entity.tags["name"] = true
  end
end

function pieces.store(tab)
  return function (entity)
    entity.store = tab
    entity.tags["store"] = true
  end
end



function pieces.system(system)
  return function (entity)
    entity.system = system
    entity.tags["system"] = true
  end
end

-- owner of the system
function pieces.owner(owner)
  return function (entity)
    entity.owner = owner
    entity.tags["owner"] = true
  end
end

function pieces.lanes(lanes)
  return function (entity)
    entity.lanes = lanes
    entity.tags["lanes"] = true
  end
end

-- used by ai
function pieces.closest(closest)
  return function (entity)
    entity.closest = closest
    entity.tags["closest"] = true
  end
end
-- -- script to be executed once after entity is created
-- -- NOTE: must be run manually!
-- function pieces.boot(func)
--   return function (entity)
--     entity.boot = func
--     entity.tags["boot"] = true
--   end
-- end

-- script to be executed each update
-- or coroutine?
function pieces.script(func)
  return function (entity)
    entity.script = func
    entity.tags["script"] = true
  end
end

function pieces.timers(timers)
  return function (entity)
    entity.timers = timers
    entity.tags["timers"] = true
  end
end

-- how rich the system is in terms of available minerals
function pieces.mining(active, richness)
  return function (entity)
    entity.mining = {
      active = active,
      richness = richness
    }
    entity.tags["mining"] = true
  end
end

-- how many people the system can hold
function pieces.population(current, limit)
  return function (entity)
    entity.population = {
      current = current,
      limit = limit
    }
    entity.tags["population"] = true
  end
end

-- list of stations around the system
-- each station is a separate entity?
function pieces.stations()
  return function (entity)
    entity.stations = {}
    entity.tags["stations"] = true
  end
end

-- list of ships bound to the system
-- each ship is a separate entity
-- ship may be far away from the system
function pieces.ships(our, enemy)
  return function (entity)
    entity.ships = {
      our = our,
      enemy = enemy,
    }
    entity.tags["ships"] = true
  end
end

return pieces