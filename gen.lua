local gen = {}

function gen.cartesian(a, b)
  local ax, ay, az = unpack(a)
  local bx, by, bz = unpack(b)
  local dx, dy, dz = bx - ax, by - ay, bz - az
  return math.sqrt(dx * dx + dy * dy + dz * dz)
end

function gen.randomPointNear(a, raduis, param)
  param = param or {}
  param.min_rho = param.min_rho or math.pi / 32
  param.max_rho = param.max_rho or math.pi / 16
  param.min_theta = param.min_theta or math.pi / 32
  param.max_theta = param.max_theta or math.pi / 16

  local ax, ay, az = unpack(a)
  -- local ar = gen.cartesian(a, {0, 0, 0})
  local ar = raduis
  local a_rho = math.atan2(ay, ax)
  local a_theta = math.acos(az / ar)

  local sign_rho = (math.random(0, 1) * 2 - 1)
  local sign_theta = (math.random(0, 1) * 2 - 1)

  -- ISSUE: rho and theta may exceed limits
  local cr = ar
  local c_rho = a_rho + sign_rho * (math.random() * param.max_rho + param.min_rho)
  local c_theta = a_theta + sign_theta * (math.random() * param.max_theta + param.min_theta)

  local cx = cr * math.sin(c_theta) * math.cos(c_rho)
  local cy = cr * math.sin(c_theta) * math.sin(c_rho)
  local cz = cr * math.cos(c_theta)

  return {cx, cy, cz}
end

function gen.generatePointNear(a, raduis, dd)
  -- TODO: compute max-min rho and theta from dd
  local min_rho = math.acos(1 - (dd * dd) / (2 * raduis * raduis))
  local max_rho = math.acos(1 - (4 * dd * dd) / (2 * raduis * raduis))
  local min_theta = math.acos(1 - (dd * dd) / (2 * raduis * raduis))
  local max_theta = math.acos(1 - (4 * dd * dd) / (2 * raduis * raduis))
  return gen.randomPointNear(a, raduis, {
    min_rho = min_rho,
    max_rho = max_rho,
    min_theta = min_theta,
    max_theta = max_theta
  })
end

function gen.generatePoints(radius, dd)
  assert(radius, "generatePoints needs radius")
  assert(dd, "generatePoints needs dd")

  -- initial metapoint
  local initial = { point = {0, 0, radius}, activity = 5 }
  local front = { initial } -- active points
  local metapoints = { initial } -- all points

  while #front > 0 do
    local current = table.remove(front, 1)

    while current.activity > 0 do
      local npoint = gen.generatePointNear(current.point, radius, dd)

      local good = true -- until too close to other point
      for _, metapoint in ipairs(metapoints) do
        if gen.cartesian(npoint, metapoint.point) < dd then
          good = false
          break -- metapoints iteration
        end
      end -- metapoints iteration

      if good then
        local nmetapoint = { point = npoint, activity = 5 }
        table.insert(front, nmetapoint)
        table.insert(metapoints, nmetapoint)
      else
        current.activity = current.activity - 1
      end
    end -- current.activity
  end

  local points = {}
  for _, metapoint in ipairs(metapoints) do
    table.insert(points, metapoint.point)
  end

  return points
end


return gen