local util = {}

function util.shuffle(list)
  local n, order, res = #list, {}, {}
   
  for i = 1, n do order[i] = { rnd = math.random(), idx = i } end
  table.sort(order, function(a, b) return a.rnd < b.rnd end)
  for i = 1, n do res[i] = list[order[i].idx] end

  return res
end

function util.seq(list)
  local k, v = nil, nil
  local fin = false

  return function ()
    if fin then return nil end
    k, v = next(list, k)
    if v == nil then fin = true end
    return v
  end
end

function util.seqinf(list)
  local k, v = nil, nil

  return function ()
    k, v = next(list, k)
    if k == nil then k, v = next(list) end
    return v
  end
end

function util.unique(list)
  local set = {}
  for _, item in ipairs(list) do
    set[item] = true
  end

  local nlist = {}
  for item, _ in pairs(set) do
    table.insert(nlist, item)
  end

  return nlist
end

function util.frand(min, max)
  return min + math.random() * (max - min)
end

function util.edist2(x1, y1, x2, y2)
  local dx = x2 - x1
  local dy = y2 - y1
  return math.sqrt(dx*dx + dy*dy)
end

function util.edist3(x1, y1, z1, x2, y2, z2)
  local dx = x2 - x1
  local dy = y2 - y1
  local dz = z2 - z1
  return math.sqrt(dx*dx + dy*dy + dz*dz)
end

function util.edist3v(from, to)
  local x1, y1, z1 = unpack(from)
  local x2, y2, z2 = unpack(to)
  local dx = x2 - x1
  local dy = y2 - y1
  local dz = z2 - z1
  return math.sqrt(dx*dx + dy*dy + dz*dz)
end

function util.interp3v(from, to, c)
  c = math.min(math.max(0, c), 1)
  local x1, y1, z1 = unpack(from)
  local x2, y2, z2 = unpack(to)
  local dx = x2 - x1
  local dy = y2 - y1
  local dz = z2 - z1

  return {x1 + c*dx, y1 + c*dy, z1 + c*dz}
end



return util