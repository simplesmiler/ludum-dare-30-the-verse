local function build(klass, ...)
  local inst = setmetatable({}, klass)
  if inst.init then inst:init(...) end
  return inst
end

local function Klass()
  local klass = {}
  klass.__index = klass
  return setmetatable(klass, { __call = build })
end

return Klass