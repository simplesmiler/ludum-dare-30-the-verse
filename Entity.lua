local Entity = Klass()

function Entity:init(id, pieces)
  assert(id, "Entity:init needs id")
  pieces = pieces or {}

  self.id = id
  self.tags = {}

  for _, piece in ipairs(pieces) do
    piece(self)
  end
end

function Entity:has(tags)
  assert(tags, "Entity:has needs tag list")

  for _, tag in ipairs(tags) do
    if not self.tags[tag] then return false end
  end

  return true
end

return Entity