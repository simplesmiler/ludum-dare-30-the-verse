local Quaternion = Klass()

function Quaternion.mul(a, b)
  local aw, ax, ay, az = unpack(a)
  local bw, bx, by, bz = unpack(b)

  local w = aw * bw - ax * bx - ay * by - az * bz
  local x = aw * bx + ax * bw + ay * bz - az * by
  local y = aw * by - ax * bz + ay * bw + az * bx
  local z = aw * bz + ax * by - ay * bx + az * bw

  return {w, x, y, z}
end

function Quaternion.mulmany(list)
  local q = list[1]

  for i = 2, #list do
    local p = list[i]
    q = Quaternion.mul(q, p)
  end

  return q
end

function Quaternion.norm(q)
  local w, x, y, z = unpack(q)
  local s = math.sqrt(w*w + x*x + y*y + z*z)
  return {w/s, x/s, y/s, z/s}
end

function Quaternion.toaxis(q)
  local w, x, y, z = unpack(q)

  local s = math.sqrt(x*x + y*y + z*z)
  if s == 0 then
    return {0, 0, 0, 0}
  else
    return {2 * math.acos(w), x/s, y/s, z/s}
  end
end

function Quaternion.conj(q)
  local w, x, y, z = unpack(q)
  return {w, -x, -y, -z}
end

function Quaternion.apply(q, v)
  local p = {0, unpack(v)}
  local qc = Quaternion.conj(q)

  local pw, px, py, pz = unpack(Quaternion.mul(Quaternion.mul(q, p), qc))
  return {px, py, pz}
end

function Quaternion.fromaxis(a)
  local aa, ax, ay, az = unpack(a)
  local s = math.sin(aa/2)

  return {math.cos(aa/2), ax*s, ay*s, az*s}
end

return Quaternion