local pieces = require("pieces")

local cMenu = Klass()

function cMenu:init(shared, conf)
  self.shared = shared
  self.conf = conf

  self.roster = Roster(Entity)
  self.roster:create("button-zen", {
    pieces.tag("logics.button"),
    pieces.layout(416, 256-32-32-8-8, 128, 32),
    pieces.label("Zen"),
    pieces.description("Observe"),
    pieces.action(function (button)
      self.shared.signalbus:emit("start", {
        avgdist = 0.15,
        player_ai = true,
        zen = true
      })
    end)
  })
  self.roster:create("button-easy", {
    pieces.tag("logics.button"),
    pieces.layout(416, 256, 128, 32),
    pieces.label("Easy"),
    pieces.description("Start small"),
    pieces.action(function (button)
      self.shared.signalbus:emit("start", {
        avgdist = 0.5,
        player_ai = false,
        zen = false
      })
    end)
  })
  self.roster:create("button-medium", {
    pieces.tag("logics.button"),
    pieces.layout(416, 256+32+8, 128, 32),
    pieces.label("Medium"),
    pieces.description("Step by step"),
    pieces.action(function (button)
      self.shared.signalbus:emit("start", {
        avgdist = 0.4,
        player_ai = false,
        zen = false
      })
    end)
  })
  self.roster:create("button-hard", {
    pieces.tag("logics.button"),
    pieces.layout(416, 256+32+8+32+8, 128, 32),
    pieces.label("Hard"),
    pieces.description("You'll reach stars"),
    pieces.action(function (button)
      self.shared.signalbus:emit("start", {
        avgdist = 0.3,
        player_ai = false,
        zen = false
      })
    end)
  })
  self.roster:create("button-custom", {
    pieces.tag("logics.button"),
    pieces.layout(416, 256+32+8+32+8+32+8, 128, 32),
    pieces.label("Custom"),
    pieces.description("Define your rules"),
    pieces.action(function (button)
      self.shared.signalbus:emit("custom")
    end)
  })

  self.mouse = {
    position = {0, 0},

    hover = nil -- button
  }

  -- body
end

function cMenu:update(dt, total)
  local buttons = self.roster:find({"logics.button"})
  local mx, my = unpack(self.mouse.position)
  self.mouse.hover = nil
  for _, button in ipairs(buttons) do
    local bx, by, bw, bh = unpack(button.layout)
    if mx > bx and mx < bx + bw and my > by and my < by + bh then
      self.mouse.hover = button
      break
    end
  end
end

function cMenu:draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.setFont(self.shared.fonts["title"])
  love.graphics.draw(self.shared.images["logo"], 960/2 - 200, 80)
  love.graphics.printf("The Verse", 0, 16, 960, "center")

  local buttons = self.roster:find({"logics.button"})
  love.graphics.setFont(self.shared.fonts["systems"])
  for _, button in ipairs(buttons) do
    local x, y, w, h = unpack(button.layout)
    love.graphics.setColor(255, 255, 255, 16)
    love.graphics.rectangle("fill", x, y, w, h)
    love.graphics.setColor(255, 255, 255, 255)

    if self.mouse.hover == button then
      love.graphics.printf(button.description, x, y, w, "center", 0, 1, 1, 0, -6)
    else
      love.graphics.printf(button.label, x, y, w, "center", 0, 1, 1, 0, -6)
    end
  end

  love.graphics.setColor(255, 255, 255, 127)
  local helptext = [[
    Use mouse to control camera (try with [shift])
    Mouse over a system to access fleet
    Drag and drop to send fleet
  ]]
  love.graphics.printf(helptext, 0, 540 - 60, 960, "center")

  love.graphics.setColor(255, 255, 255, math.random(8))
  love.graphics.printf("Denis Karabaza @ Ludum Dare 30", 0, 60, 960, "center")
end


function cMenu:keypressed(key, isrepeat)
  if key == "escape" then
    love.event.quit()
  end
end

function cMenu:mousepressed(x, y, button)
  if button == "l" then
    local buttons = self.roster:find({"logics.button"})
    for _, button in ipairs(buttons) do
      local bx, by, bw, bh = unpack(button.layout)
      if x > bx and x < bx + bw and y > by and y < by + bh then
        if button.action then button.action(button) end
        break
      end
    end
  end
end

function cMenu:mousereleased(x, y, button)
end

function cMenu:mouseposition(x, y)
  self.mouse.position = {x, y}
end

return cMenu