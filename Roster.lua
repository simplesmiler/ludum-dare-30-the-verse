local Roster = Klass()

function Roster:init(Entity)
  self._Entity = Entity
  self._roster = {}
end

function Roster:reg(entity)
  assert(entity, "Roster:reg needs entity")

  -- TODO: check if id is already reserved
  self._roster[entity.id] = entity
end

function Roster:unreg(entity)
  assert(entity, "Roster:remove needs entity")

  -- TODO: check if entity registered
  local id = entity.id
  self._roster[entity.id] = nil
end

function Roster:create(...)
  local entity = self._Entity(...)
  self:reg(entity)
  return entity
end

function Roster:get(id)
  assert(id, "Roster:get needs id")

  return self._roster[id]
end

function Roster:find(tags)
  assert(tags, "Roster:find needs tags")

  local entities = {}
  for id, entity in pairs(self._roster) do
    if entity:has(tags) then table.insert(entities, entity) end
  end

  return entities
end

-- TODO: implement
function Roster:itfind(tags)
  assert(tags, "Roster:find needs tags")

  assert(not "Not implemented")
end

return Roster